from django.urls import path
from . import views

app_name = "loginPage"

urlpatterns = [
	path('', views.index, name = 'index'),
    path('login/', views.loginFunction, name = 'login'),
    path('logout/', views.logoutFunction, name = 'logout'),
    path('register/', views.registerFunction, name = 'register'),
]
