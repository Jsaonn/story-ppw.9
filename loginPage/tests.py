from django.test import TestCase, Client
from django.urls import resolve
from .views import index
from django.http import HttpRequest
from django.contrib.auth.models import User

# Create your tests here.

class Test(TestCase):
    def test_app_url_ada(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_login_url_ada(self):
        response = Client().get('/login/')
        self.assertEqual(response.status_code, 200)

    def test_register_url_ada(self):
        response = Client().get('/register/')
        self.assertEqual(response.status_code, 200)

    def test_app_ada(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    def test_template_landpage(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'index.html')

    def test_template_login(self):
        response = Client().get('/login/')
        self.assertTemplateUsed(response, 'login.html')

    def test_template_register(self):
        response = Client().get('/register/')
        self.assertTemplateUsed(response, 'register.html')

    def test_landing_page(self):
        self.assertIsNotNone(index)

    def test_register_berhasil(self):
        counter = User.objects.all().count()
        context = {
            'username' : 'jsaon',
            'password1' : 'passwordsatu',
            'password2' : 'passwordsatu'
        }
        response = self.client.post('/register/', data = context)
        self.assertEqual(User.objects.all().count(), counter + 1)

    def test_login_berhasil(self):
        context = {
            'username' : 'jsaon',
            'password' : 'passwordsatu'
        }
        response = self.client.post('/login/', data = context)
        self.assertEqual(response.status_code, 200)

    def test_logout_berhasil(self):
        self.client.login(username = 'jsaon', password = 'passwordsatu')
        response = self.client.get('/logout/')
        self.assertEqual(response.status_code, 302)