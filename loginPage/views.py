from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from django.contrib import messages
from .forms import *

# Create your views here.

def index(request):
    response = {'user': request.user}
    return render(request, 'index.html', response)

def loginFunction(request):
    form = LoginForm()
    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            username = request.POST.get('username')
            password = request.POST.get('password')
            user = authenticate(request, username=username, password=password)

            if user is not None:
                login(request, user)
                return redirect('loginPage:index')
            else:
                messages.error(request, 'invalid username or password')

    context = {'form' : form}
    return render(request, 'login.html', context)

def registerFunction(request):
    form = UserCreationForm()
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            messages.success(request, 'Account successfully created!')
            return redirect('loginPage:login')
    
    context = {'form' : form}
    return render(request, 'register.html', context)

def logoutFunction(request):
    logout(request)
    return redirect('loginPage:login')