from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from django.forms import ModelForm

class RegisterForm(UserCreationForm):
    class Meta:
        model = User
        fields = ['username', 'password1']

class LoginForm(forms.Form):
    username = forms.CharField(widget = forms.TextInput(attrs={
        'class' : 'form-control',
        'placeholder' : 'input name',
        'type' : 'text',
        'required' : True
    }))

    password = forms.CharField(widget = forms.PasswordInput(attrs={
        'class' : 'form-control',
        'placeholder' : 'input password',
        'type' : 'password',
        'required' : True
    }))